We are a multi specialty dental group practice in Upland, Rancho Cucamonga, Chino and Wildomar. We have family dentists, orthodontists, endodontist, periodontist, dental implant specialist, oral surgery and dental anesthesiologist.

Address: 1268 W Foothill Blvd, Upland, CA 91786, USA

Phone: 909-981-4111

Website: http://www.myuplanddental.com
